#pragma once

#include "glm\glm.hpp"

struct BoundingBox {
	glm::vec3 center;
	glm::vec3 lower_left;
	float height;
	float width;
	float x;
	float y;

	bool is_point_inside_box(float x, float y);
	bool collides_with_other_box(BoundingBox other);
};