#version 400
layout(location=0) in vec4 position;
layout(location=1) in vec4 color;
out vec4 exColor;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
void main(void)
{
exColor = color;
gl_Position = proj * view * model * position;
};