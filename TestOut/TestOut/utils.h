#ifndef LEARN_ENGINE_UTILS
#define LEARN_ENGINE_UTILS

char *concat_strings(char *lhs, size_t lhs_size, char *rhs, size_t rhs_size);
const char *read_to_end(std::string filePath);

template <typename T> int sgn(T val)
{
	return (T(0) < val) - (val < T(0));
}

#endif