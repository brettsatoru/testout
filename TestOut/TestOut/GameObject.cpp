#include "GameObject.h"
#include <mutex>

static unsigned int currentAvailableId = 0;
static std::mutex idMutex;
static unsigned int GetGameObjectId()
{
	std::lock_guard<std::mutex> guard(idMutex);
	return ++currentAvailableId;
}

GameObject::GameObject()
{
	// Sets nothing.
	pVelocity = new Transform();

	pSceneNode = 0;
	pUpdate = 0;
	pColliderEnter = 0;
	pColliderExit = 0;

	mId = GetGameObjectId();
}

SceneNode *GameObject::GenerateSceneNode(MeshRenderer *mesh, Transform *transform, Render::Material *material)
{
	SceneNode *oldNode = pSceneNode;

	pSceneNode = new SceneNode(transform, mesh, material);

	return oldNode;
}

unsigned int GameObject::GetGameObjectId()
{
	return mId;
}

SceneNode *GameObject::GetSceneNode()
{
	return pSceneNode;
}

SceneNode *GameObject::SetSceneNode(SceneNode *node)
{
	SceneNode *oldNode = pSceneNode;

	pSceneNode = node;

	return oldNode;
}

UpdateNode *GameObject::SetUpdateLogic(UpdateNode *node)
{
	UpdateNode *oldUpdate = pUpdate;

	pUpdate = node;

	return oldUpdate;
}

ColliderNode *GameObject::GetCollisionEnterLogic()
{
	return pColliderEnter;
}

ColliderNode *GameObject::GetCollisionExitLogic()
{
	return pColliderExit;
}

ColliderNode *GameObject::SetCollisionEnterLogic (ColliderNode *node)
{
	ColliderNode *oldCollide = pColliderEnter;
	if (oldCollide != NULL)
	{
		oldCollide->SetSceneNode(NULL);
	}

	pColliderEnter = node;
	pColliderEnter->SetSceneNode(pSceneNode);

	return oldCollide;
}

ColliderNode *GameObject::SetCollisionExitLogic(ColliderNode *node)
{
	ColliderNode *oldCollide = pColliderExit;
	if (oldCollide != NULL)
	{
		oldCollide->SetSceneNode(NULL);
	}

	pColliderExit = node;

	return oldCollide;
}

Transform *GameObject::GetVelocity()
{
	return pVelocity;
}

void GameObject::Update(float dt)
{
	if (pUpdate != 0)
	{
		pUpdate->HandleUpdate(pSceneNode, dt);
	}
}

void GameObject::LateUpdate(float dt)
{
	if (!mCollidingObjects.empty())
	{
		for (auto iter = mCollidingObjects.begin(); iter != mCollidingObjects.end();)
		{
			if (!iter->second->GetSceneNode()->CollidesWith(GetSceneNode()))
			{
				CollisionExit(iter->second, glm::vec3(0.f));
				iter = mCollidingObjects.erase(iter);
			}
			else
			{
				iter++;
			}
		}
	}
}

void GameObject::CollisionEnter(GameObject *other, glm::vec3 intersectPoint)
{
	if (mCollidingObjects[other->GetGameObjectId()] != 0) return;
	if (pColliderEnter != 0 && other->GetCollisionEnterLogic() != 0)
	{
		pColliderEnter->HandleCollisionEvent(pColliderEnter, other->GetCollisionEnterLogic(), intersectPoint);
		mCollidingObjects[other->GetGameObjectId()] = other;
	}
}

void GameObject::CollisionExit(GameObject *other, glm::vec3 intersectPoint)
{
	if (pColliderExit != 0 && other->GetCollisionExitLogic() != 0)
	{
		pColliderExit->HandleCollisionEvent(pColliderExit, other->GetCollisionExitLogic(), intersectPoint);
	}
}