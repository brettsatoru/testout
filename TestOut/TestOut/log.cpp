#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <thread>
#include <iostream>

#include "log.h"
#include "utils.h"

void Logger::log(char *message) {
	for (auto index = 0; index < LogThreadBufferSize; index++) {
		if (this->logThreadsFinished[index]) {
			free(this->logThreads[index]);
			this->logThreadsFinished[index] = false;
		}
	}

	auto length = strlen(message);

	LogData data = { &this->logLock, this->filePath, message, length, true, &this->numberOfThreads, &this->logThreadsFinished[this->numberOfThreads] };

	this->logThreads[this->numberOfThreads] = (std::thread*)malloc(sizeof(std::thread));
	auto logThread = std::thread(log_to_file, data);
	logThread.detach();
	memcpy(this->logThreads[this->numberOfThreads], &logThread, sizeof(logThread));

	this->logLock.lock();
	this->numberOfThreads += 1;
	this->logLock.unlock();
}

Logger::~Logger() {
	for (auto index = 0; index < LogThreadBufferSize; index++) {
		if (this->logThreadsFinished[index]) {
			free(this->logThreads[index]);
			this->logThreadsFinished[index] = false;
		}
	}

	while (this->numberOfThreads > 0) {
	}
}

void Logger::log_line(char *message) {
	for (auto index = 0; index < LogThreadBufferSize; index++) {
		if (this->logThreadsFinished[index]) {
			free(this->logThreads[index]);
			this->logThreadsFinished[index] = false;
		}
	}

	auto length = strlen(message);

	char *buffer = (char *)malloc(length + 2);
	snprintf(buffer, length + 2, "%s\n", message);

	LogData data = { &this->logLock, this->filePath, buffer, length + 1, true, &this->numberOfThreads, &this->logThreadsFinished[this->numberOfThreads] };

	this->logThreads[this->numberOfThreads] = (std::thread*)malloc(sizeof(std::thread));
	auto logThread = std::thread(log_to_file, data);
	logThread.detach();
	memcpy(this->logThreads[this->numberOfThreads], &logThread, sizeof(logThread));

	this->logLock.lock();
	this->numberOfThreads += 1;
	this->logLock.unlock();
}

void log_to_file(LogData data) {
	data.logLock->lock();

	FILE *fileOut;
	fopen_s(&fileOut, data.filePath, "a");
	fwrite(data.message, sizeof(char), data.messageLength, fileOut);

	if (data.freeMessage) {
		free(data.message);
	}

	fclose(fileOut);

	*data.threadFinished = true;
	*data.numberOfThreads = *data.numberOfThreads - 1;
	
	data.logLock->unlock();
}

void log(LogLevel level, char *message) {
	char *messageHeader;

	switch (level) {
	case Trace:
		messageHeader = "[TRACE] ";
		break;
	case Info:
		messageHeader = "[INFO ] ";
		break;
	case Debug:
		messageHeader = "[DEBUG] ";
		break;
	case Warn:
		messageHeader = "[WARN ] ";
		break;
	case Error:
		messageHeader = "[ERROR] ";
		break;
	case Fatal:
		messageHeader = "[FATAL] ";
		break;
	}

	auto now = time(0);
	auto nowProcessed = ctime(&now);
	auto timeLength = strlen(nowProcessed);
	nowProcessed[timeLength - 1] = '\t'; // Replace \n with \t.

	messageHeader = concat_strings(messageHeader, HeaderLength, nowProcessed, timeLength);
	auto headerLength = strlen(messageHeader);
	auto length = strlen(message);

	char *toPrint = concat_strings(messageHeader, headerLength, message, length);

	logger.log_line(toPrint);
	std::cout << toPrint << std::endl;
}
