#include "FlatColorMaterial.h"

FlatColorMaterial::FlatColorMaterial(GLuint program) : Render::Material(program)
{
	mFlatColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
}

FlatColorMaterial::FlatColorMaterial(GLuint program, glm::vec4 color) : Render::Material(program)
{
	mFlatColor = color;
}

void FlatColorMaterial::LoadMaterialUniforms()
{
	LoadUniform("flatColor", mFlatColor);
}

glm::vec4 FlatColorMaterial::GetFlatColor()
{
	return mFlatColor;
}

void FlatColorMaterial::SetFlatColor(glm::vec4 color)
{
	mFlatColor = color;
}