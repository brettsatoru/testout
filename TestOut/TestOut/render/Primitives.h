#ifndef ENGINE_PRIMITIVES
#define ENGINE_PRIMITIVES

#include "GL\glew.h"
#include <GL\GL.h>
#include "MeshRenderer.h"

/// A file responsible for GL Primitive Definitions, such as Quad, Cube, Pyramid, Cylinder, etc.
/// This file will grow as we get more primitives we need to work with.

static MeshRenderer *Quad;

MeshRenderer *GetQuad();

#endif
