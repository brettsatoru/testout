#ifndef RENDER_MATERIAL
#define RENDER_MATERIAL

#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "..\glm\glm.hpp"
#include "..\glm\gtc\type_ptr.hpp"

namespace Render
{

	class Material
	{
	protected:
		GLuint shaderprogram;
	public:
		Material(GLuint program);

		void LoadProgram();
		virtual void LoadMaterialUniforms() = 0;

		int GetProgramId();

		void LoadUniform(char *uniformName, float value);
		void LoadUniform(char *uniformName, glm::vec4 value);
		void LoadUniform(char *uniformName, glm::mat4 value);
	};
}
#endif