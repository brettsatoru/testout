#include "MeshRenderer.h"
#include <string.h>
#include <stdlib.h>

// Constructor, verts and vertCount required.
MeshRenderer::MeshRenderer(const Vertex *verts, size_t vertCount, const GLubyte *indices, size_t indexCount)
{
	// TODO: we need an error here.
	// We need a number of vertices in the pointer and a vertex count for mapping purposes.
	if (verts == 0) return;
	if (vertCount == 0) return;

	pVertexArray = (Vertex *)malloc(vertCount * sizeof(Vertex));
	memcpy(pVertexArray, verts, vertCount * sizeof(Vertex));
	mVertexCount = vertCount;
	mIndexCount = indexCount;

	// Now to set the index buffer if it is available.
	// Set Index Buffer to null address
	mIndexBuffer = 0;

	const size_t VertexSize = sizeof(pVertexArray[0]);
	const size_t PositionSize = sizeof(pVertexArray[0].XYZW);
	const size_t ColorOffset = sizeof(pVertexArray[0].XYZW);
	const size_t ColorSize = sizeof(pVertexArray[0].RGBA);

	GLenum ErrorCheckValue = glGetError();

	glGenVertexArrays(1, &mGLVertexArray);
	glBindVertexArray(mGLVertexArray);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		// Log an error
		// Destroy created buffers, if applicable.
		return;
	}

	// Loading Vertices.
	glGenBuffers(1, &mVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, mVertexCount * sizeof(Vertex), pVertexArray, GL_STATIC_DRAW);
	
	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		// Log an error
		// Destroy created buffers, if applicable.
		return;
	}

	// Load Vertices into Vertex Array Object
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, VertexSize, 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, VertexSize, (GLvoid *)ColorOffset);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		// Log an error
		// Destroy created buffers, if applicable.
		return;
	}

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);


	// Unbind generated buffers before we might return.
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		// Log an error
		// Destroy created buffers, if applicable.
		return;
	}

	// Not actually an error, this information is optional.
	if (indices == 0) return;
	// TODO: This IS an error if we're given indices to work with, log an error,
	// clear the rest of the buffers as if the user sent in an index array with a vertex array the draw 
	// behavior is undefined if the index count isn't available.
	if (indexCount == 0) return;

	pIndexArray = (GLubyte*)malloc(indexCount);
	memcpy(pIndexArray, indices, indexCount);

	// Rebind the vertex buffer object
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);

	// Bind the index buffer
	glGenBuffers(1, &mIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndexCount, pIndexArray, GL_STATIC_DRAW);

	// Unbind Buffers.
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		// Log an error.
		return;
	}

}

// Destructor
MeshRenderer::~MeshRenderer()
{
	// Unbind all buffers.
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glDeleteVertexArrays(1, &mGLVertexArray);
	glDeleteBuffers(1, &mIndexBuffer);
	glDeleteBuffers(1, &mVertexBuffer);

	free(pVertexArray);
	if (mIndexCount > 0)
	{
		free(pIndexArray);
	}
}

// Gets a vertex in the mesh array
Vertex *MeshRenderer::GetVertexByIndex(int index)
{
	if (index < 0)
	{
		// Log error and return null address
		return 0;
	}

	if (index >= mVertexCount)
	{
		// Log error and return null address
		return 0;
	}

	// Otherwise return the address of the targetted vertice
	return &pVertexArray[index];
}

// Sets a vertex in the mesh array
void MeshRenderer::SetVertexByIndex(int index, Vertex *value)
{
	if (index < 0 || index >= mVertexCount)
	{
		return;
	}

	pVertexArray[index].X = value->X;
	pVertexArray[index].Y = value->Y;
	pVertexArray[index].Z = value->Z;
	pVertexArray[index].W = value->W;

	pVertexArray[index].R = value->R;
	pVertexArray[index].G = value->G;
	pVertexArray[index].B = value->B;
	pVertexArray[index].A = value->A;

	mIsDirty = true;
}

GLenum MeshRenderer::GetDrawType()
{
	return mGLDrawType;
}

void MeshRenderer::SetDrawType(GLenum drawType)
{
	mGLDrawType = drawType;
}

void MeshRenderer::UpdateMesh()
{
	if (!mIsDirty) return;

	const size_t VertexSize = sizeof(pVertexArray[0]);
	const size_t PositionSize = sizeof(pVertexArray[0].XYZW);
	const size_t ColorOffset = sizeof(pVertexArray[0].XYZW);
	const size_t ColorSize = sizeof(pVertexArray[0].RGBA);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, mVertexCount * sizeof(Vertex), pVertexArray, GL_STATIC_DRAW);

	glBindVertexArray(mGLVertexArray);
	glVertexAttribPointer(0, PositionSize, GL_FLOAT, GL_FALSE, VertexSize, 0);
	glVertexAttribPointer(1, ColorSize, GL_FLOAT, GL_FALSE, VertexSize, (GLvoid *)ColorOffset);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	mIsDirty = false;
}

void MeshRenderer::Draw()
{
	int ErrorCheckValue = glGetError();

	// Bind our Vertex Array and our Index Array
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBindVertexArray(mGLVertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);

	if (mIndexBuffer != 0)
	{
		glDrawElements(mGLDrawType, mIndexCount, GL_UNSIGNED_BYTE, (GLvoid*)0);
	}
	else
	{
		glDrawArrays(mGLDrawType, 0, mVertexCount * sizeof(Vertex));
	}

	// Unbind our Vertex Array buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		return;
	}
}