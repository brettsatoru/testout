#include "Material.h"

Render::Material::Material(GLuint program)
{
	shaderprogram = program;
}

void Render::Material::LoadProgram()
{
	glUseProgram(shaderprogram);
	LoadMaterialUniforms();
}

int Render::Material::GetProgramId()
{
	return shaderprogram;
}

void Render::Material::LoadUniform(char * uniformName, float value)
{
	GLuint uniformId = glGetUniformLocation(shaderprogram, uniformName);
	glUniform1f(uniformId, value);
}

void Render::Material::LoadUniform(char * uniformName, glm::vec4 value)
{
	GLuint uniformId = glGetUniformLocation(shaderprogram, uniformName);
	//glUniform4f(uniformId, value.x, value.y, value.z, value.w);
	glUniform4fv(uniformId, 1, glm::value_ptr(value));
}

void Render::Material::LoadUniform(char * uniformName, glm::mat4 value)
{
	GLuint uniformId = glGetUniformLocation(shaderprogram, uniformName);
	glUniformMatrix4fv(uniformId, 1, GL_FALSE, glm::value_ptr(value));
}