#ifndef FLATCOLOR_MATERIAL
#define FLATCOLOR_MATERIAL

#include "Material.h"
#include "../glm/glm.hpp"

class FlatColorMaterial final : public Render::Material
{
private:
	glm::vec4 mFlatColor;
public:
	FlatColorMaterial(GLuint program, glm::vec4 color);
	FlatColorMaterial(GLuint program);

	void LoadMaterialUniforms();

	glm::vec4 GetFlatColor();
	void SetFlatColor(glm::vec4 flatColor);
};

#endif