#include "Transform.h"
#include "..\glm\gtx\transform.hpp"

Transform::Transform()
{
	this->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	this->SetRotation(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));
	this->SetScale(glm::vec3(1.0f, 1.0f, 1.0f));
	transformMatrix = glm::mat4(1.0f);
}

Transform::Transform(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
{
	this->SetPosition(position);
	this->SetRotation(rotation);
	this->SetScale(scale);
	transformMatrix = glm::mat4(1.0f);
}

void Transform::Move(glm::vec3 moveVector)
{
	SetPosition(mPosition + moveVector);
}

void Transform::Move(float x, float y, float z)
{
	glm::vec3 moveVector(x, y, z);
	Move(moveVector);
}

void Transform::SetPosition(glm::vec3 newPosition)
{
	mPosition = newPosition;
	// may create a callback later to inform some other source that the position is about to be/has changed.
}

void Transform::SetPosition(float x, float y, float z)
{
	glm::vec3 newPosition(x, y, z);
	SetPosition(newPosition);
}

glm::vec3 Transform::GetPosition()
{
	return this->mPosition;
}

void Transform::Rotate(glm::quat rotVector)
{
	SetRotation(this->mqRotation * rotVector);
}

void Transform::Rotate(glm::vec3 rotVector)
{
	Rotate(glm::quat(rotVector));
}

void Transform::Rotate(float x, float y, float z)
{
	Rotate(glm::quat(glm::vec3(x, y, z)));
}

void Transform::SetRotation(glm::quat newRotation)
{
	this->mqRotation = newRotation;
}

void Transform::SetRotation(float x, float y, float z)
{
	this->mqRotation = glm::quat(glm::vec3(x, y, z));
}

glm::quat Transform::GetRotation()
{
	return this->mqRotation;
}

glm::vec3 Transform::GetEulerRotation()
{
	return glm::eulerAngles(this->mqRotation);
}

void Transform::Resize(glm::vec3 resizeVector)
{
	SetScale(this->mScale + resizeVector);
}

void Transform::Resize(float x, float y, float z)
{
	Resize(glm::vec3(x, y, z));
}

void Transform::SetScale(glm::vec3 newScale)
{
	this->mScale = newScale;
}

void Transform::SetScale(float x, float y, float z)
{
	SetScale(glm::vec3(x, y, z));
}

glm::vec3 Transform::GetScale()
{
	return this->mScale;
}

glm::mat4 Transform::CalculateTransformMatrix() {
	glm::mat4 scale = glm::scale(mScale);
	glm::mat4 rot = glm::mat4(mqRotation);
	glm::mat4 trans = glm::translate(mPosition);

	return trans * rot * scale;
}

void Transform::SetGLMatrix(GLint uniformId)
{
	transformMatrix = CalculateTransformMatrix();

	glUniformMatrix4fv(uniformId, 1, GL_FALSE, glm::value_ptr(transformMatrix));
}

void Transform::PopGLMatrix(GLint uniformId)
{
	// Reset the model matrix to identity (so we don't dirty the next matrix)
	glUniformMatrix4fv(uniformId, 1, GL_FALSE, glm::value_ptr(glm::mat4(1.0f)));
}
