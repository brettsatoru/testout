#ifndef ENLIGHT_TRANSFORM
#define ENLIGHT_TRANSFORM

#include <GL\glew.h>
#include <GL\GL.h>
#include "..\glm\glm.hpp"
#include "..\glm\gtx\quaternion.hpp"
#include "..\glm\gtc\functions.hpp"
#include "..\glm\gtc\matrix_transform.hpp"
#include "..\glm\gtc\type_ptr.hpp"
#include <stack>

class Transform {
private:
	glm::vec3 mPosition;
	glm::quat mqRotation;
	glm::vec3 mScale;
	glm::mat4 transformMatrix;
public:
	Transform();
	Transform(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);

	void Move(glm::vec3 moveVector);
	void Move(float x, float y, float z);
	void SetPosition(glm::vec3 newPosition);
	void SetPosition(float x, float y, float z);

	void Rotate(glm::quat rotVector);
	void Rotate(glm::vec3 rotVector);
	void Rotate(float x, float y, float z);
	void SetRotation(glm::quat newRotation);
	void SetRotation(float x, float y, float z);

	void Resize(glm::vec3 resizeVector);
	void Resize(float x, float y, float z);
	void SetScale(glm::vec3 newScale);
	void SetScale(float x, float y, float z);

	glm::vec3 GetPosition();
	glm::quat GetRotation();
	glm::vec3 GetEulerRotation();
	glm::vec3 GetScale();
	glm::mat4 CalculateTransformMatrix();

	void SetGLMatrix(GLint uniformId);
	void PopGLMatrix(GLint uniformId);
};

#endif