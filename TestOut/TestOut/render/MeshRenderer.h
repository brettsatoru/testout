#ifndef  MESH_RENDERER
#define MESH_RENDERER
#include "GL\glew.h"

struct Vertex
{
	union {
		GLfloat XYZW[4];
		struct {
			GLfloat X;
			GLfloat Y;
			GLfloat Z;
			GLfloat W;
		};
	};

	union {
		GLfloat RGBA[4];
		struct {
			GLfloat R;
			GLfloat G;
			GLfloat B;
			GLfloat A;
		};
	};
};

class MeshRenderer
{
private:
	size_t mVertexCount;
	Vertex *pVertexArray;
	size_t mIndexCount;
	GLubyte *pIndexArray;

	GLuint mVertexBuffer, mIndexBuffer;
	GLuint mGLVertexArray;
	GLenum mGLDrawType;
	bool mIsDirty;
public:
	MeshRenderer(const Vertex *verts, size_t vertCount, const GLubyte *indices = 0, size_t indexCount = 0);
	~MeshRenderer();

	Vertex *GetVertexByIndex(int index);
	void SetVertexByIndex(int index, Vertex *value);

	GLenum GetDrawType();
	void SetDrawType(GLenum drawType);

	void UpdateMesh();

	void Draw();
};
#endif