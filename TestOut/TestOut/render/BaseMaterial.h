#ifndef MATERIAL_BASE
#define MATERIAL_BASE

#include "Material.h"

class BaseMaterial final : public Render::Material
{
public:
	BaseMaterial(GLuint program);
	void LoadMaterialUniforms();
};

#endif
