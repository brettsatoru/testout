#include "Primitives.h"

MeshRenderer *GetQuad()
{
	if (Quad == 0)
	{
		Vertex vertices[] =
		{
			{ {-1.f,  1.f, 0.f, 1.f}, {1.f, 1.f, 1.f, 1.f} }, // Upper-Left
			{ { 1.f,  1.f, 0.f, 1.f}, {1.f, 1.f, 1.f, 1.f} }, // Upper-Right
			{ {-1.f, -1.f, 0.f, 1.f}, {1.f, 1.f, 1.f, 1.f} }, // Lower-Left
			{ { 1.f, -1.f, 0.f, 1.f}, {1.f, 1.f, 1.f, 1.f} }  // Lower-Right
		};
		GLubyte indices[] = {0, 1, 2, 2, 1, 3};

		auto num_vertices = sizeof(vertices) / sizeof(Vertex);

		Quad = new MeshRenderer(vertices, num_vertices, indices, sizeof(indices));
		Quad->SetDrawType(GL_TRIANGLE_STRIP);
	}

	return Quad;
}