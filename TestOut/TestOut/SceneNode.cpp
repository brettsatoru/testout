#include "SceneNode.h"
#include <mutex>

SceneNode::SceneNode(Transform * transform, MeshRenderer * renderer, Render::Material * material)
{
	mTransform = transform;
	mMeshRenderer = renderer;
	mMaterial = material;
	if (mMaterial != 0)
	{
		mShaderProgramId = mMaterial->GetProgramId();
	}
	SetVisibility(true);
}

SceneNode::~SceneNode()
{
	mTransform = 0; mMeshRenderer = 0; mShaderProgramId = 0;
}

Transform *SceneNode::transform()
{
	return mTransform;
}

MeshRenderer SceneNode::sharedmesh()
{
	return *mMeshRenderer;
}

GLuint SceneNode::GetShaderId()
{
	return mShaderProgramId;
}

GLuint SceneNode::SetShaderId(GLuint id)
{
	GLuint old = mShaderProgramId;
	mShaderProgramId = id;
	return old;
}

bool SceneNode::IsVisible()
{
	return mVisible;
}

void SceneNode::SetVisibility(bool target)
{
	if (mMeshRenderer != 0)
	{
		mVisible = target;
	}
	else
	{
		// We will change this when we have a hierarchy to also consider children.  This is to save on draw calls.
		mVisible = false;
	}
}

void SceneNode::Draw(glm::mat4 projection, glm::mat4 view, glm::mat4 parentModel)
{
	if (mVisible == false) return;

	if (mShaderProgramId == 0) return;

	// Handle Child draw calls before your own

	if (mMeshRenderer == 0) return;

	if (mMaterial != 0)
	{
		mMaterial->LoadProgram();

		mMaterial->LoadUniform("proj", projection);
		mMaterial->LoadUniform("view", view);
	} else {
		glUseProgram(mShaderProgramId);

		// get the uniformIds
		GLint projId = glGetUniformLocation(mShaderProgramId, "proj");
		GLint viewId = glGetUniformLocation(mShaderProgramId, "view");

		glUniformMatrix4fv(projId, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(viewId, 1, GL_FALSE, glm::value_ptr(view));
	}

	GLint modelId = glGetUniformLocation(mShaderProgramId, "model");

	mTransform->SetGLMatrix(modelId);
	mMeshRenderer->Draw();
	mTransform->PopGLMatrix(modelId);
}

BoundingBox SceneNode::GetBoundingBox() {
	BoundingBox box;
	glm::mat4 transform_matrix = mTransform->CalculateTransformMatrix();

	auto index = 0;
	auto current_vertex = mMeshRenderer->GetVertexByIndex(index);

	auto min_x = FLT_MAX;
	auto max_x = FLT_MIN;
	auto min_y = FLT_MAX;
	auto max_y = FLT_MIN;
	auto min_z = FLT_MAX;
	auto max_z = FLT_MIN;

	while (current_vertex) {
		auto vertex_matrix = glm::vec4{ current_vertex->X, current_vertex->Y, current_vertex->Z, current_vertex->W };
		auto transformed_vertex = transform_matrix * vertex_matrix;

		if (transformed_vertex.x < min_x) { min_x = transformed_vertex.x; }
		if (transformed_vertex.x > max_x) { max_x = transformed_vertex.x; }
		if (transformed_vertex.y < min_y) { min_y = transformed_vertex.y; }
		if (transformed_vertex.y > max_y) { max_y = transformed_vertex.y; }
		if (transformed_vertex.z < min_z) { min_z = transformed_vertex.z; }
		if (transformed_vertex.z > max_z) { max_z = transformed_vertex.z; }

		index++;
		current_vertex = mMeshRenderer->GetVertexByIndex(index);
	}

	box.lower_left.x = min_x;
	box.lower_left.y = min_y;
	box.lower_left.z = min_z;
	box.height = max_y - min_y;
	box.width = max_x - min_x;

	box.center = glm::vec3{ (box.lower_left.x + (box.width / 2)), (box.lower_left.y + (box.height / 2)), 1 };

	return box;
}

bool SceneNode::MaybeCollide(SceneNode *maybeCollidesWith, int x, int y) {
	auto box = GetBoundingBox();
	if (box.is_point_inside_box(x, y)) {
		return true;
	}

	return false;
}

bool SceneNode::CollidesWith(SceneNode *otherNode)
{
	return GetBoundingBox().collides_with_other_box(otherNode->GetBoundingBox());
}

unsigned int SceneNode::GetNodeId()
{
	return mId;
}