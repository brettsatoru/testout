#ifndef BALL_LOGIC
#define BALL_LOGIC

#include "..\SceneNode.h"
#include "UpdateNode.h"
#include "ColliderNode.h"
#include "..\render\Transform.h"
#include "..\render\MeshRenderer.h"
#include "..\glm\glm.hpp"

class BallUpdate : public UpdateNode
{
private:
	bool mLaunched;
	Transform *pVelocity;
	SceneNode *pTrackedObject;
public:
	BallUpdate(Transform *);
	void HandleUpdate(SceneNode *, float);

	Transform *GetVelocity();
	bool GetIsLaunched();
	void SetLaunched(bool);

	SceneNode *GetTrackedObject();
	void SetTrackedObject(SceneNode *);
};

class BallCollide : public ColliderNode
{
private:
	Transform *pVelocity;
public:
	BallCollide(Transform *);
	void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3);

	Transform *GetVelocity();
};

#endif