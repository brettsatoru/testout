#include "BorderLogic.h"

void CeilingLogic::HandleCollisionEvent(ColliderNode *me, ColliderNode *other, glm::vec3 intersectPoint)
{
	BallCollide *ballObj = dynamic_cast<BallCollide*>(other);

	// If not a ball, nothing to handle
	if (ballObj == NULL) return;

	// Else, divert the ball
	glm::vec3 currVelocity = ballObj->GetVelocity()->GetPosition();

	currVelocity.y = -currVelocity.y;

	ballObj->GetVelocity()->SetPosition(currVelocity);
}

void WallLogic::HandleCollisionEvent(ColliderNode *me, ColliderNode *other, glm::vec3 intersectPoint)
{
	BallCollide *ballObj = dynamic_cast<BallCollide*>(other);

	if (ballObj == NULL) return;

	glm::vec3 currVelocity = ballObj->GetVelocity()->GetPosition();

	currVelocity.x = -currVelocity.x;

	ballObj->GetVelocity()->SetPosition(currVelocity);
}

void FloorLogic::HandleCollisionEvent(ColliderNode *me, ColliderNode *other, glm::vec3 intersectPoint)
{
	BallCollide *ballObj = dynamic_cast<BallCollide*>(other);

	if (ballObj == NULL) return;

	if (bUNode != NULL)
	{
		bUNode->SetLaunched(false);
	}
}

void FloorLogic::SetBallUpdateTarget(BallUpdate *tracker)
{
	bUNode = tracker;
}