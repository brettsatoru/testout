#include "ColliderNode.h"

ColliderNode::ColliderNode()
{
	viewNode = new SceneNode;
}

SceneNode *ColliderNode::GetSceneNode()
{
	return viewNode;
}

void ColliderNode::SetSceneNode(SceneNode *node)
{
	viewNode = node;
}