#include "PaddleLogic.h"
#include "BallLogic.h"

#include "..\utils.h"

PaddleUpdate::PaddleUpdate(double *mX)
{
	mouseX = mX;
}

void PaddleUpdate::HandleUpdate(SceneNode *node, float dt)
{
	glm::vec3 currPosition = node->transform()->GetPosition();

	currPosition.x = (float)(*mouseX);

	node->transform()->SetPosition(currPosition);
}

void PaddleCollide::HandleCollisionEvent(ColliderNode *self, ColliderNode *other, glm::vec3 intersectPoint)
{
	BallCollide * ballObj = dynamic_cast<BallCollide *>(other);

	if (ballObj == NULL) return;

	float width = self->GetSceneNode()->GetBoundingBox().width / 2;
	glm::vec3 origin = self->GetSceneNode()->transform()->GetPosition();
	glm::vec3 ballPoint = other->GetSceneNode()->transform()->GetPosition();
	glm::vec3 currVel = ballObj->GetVelocity()->GetPosition();
	float speed = glm::length(ballObj->GetVelocity()->GetPosition()) + 5.0f;

	glm::vec3 normVel = glm::normalize(currVel);
	normVel.y = abs(normVel.y);
	int velSign = sgn(normVel.x);
	normVel.x = abs(normVel.x);

	float reflectedAngle = (normVel.x != 0) ? atan(normVel.y / normVel.x) : 90.f;
	reflectedAngle = (velSign == -1) ? reflectedAngle + 90.f : reflectedAngle;

	float relPointOfImpact = ballPoint.x - origin.x;
	int poiSign = sgn(relPointOfImpact);
	relPointOfImpact = abs(relPointOfImpact);

	float pcImpact = (relPointOfImpact / width) * 100.f;
	if (pcImpact > 95.f)
	{
		reflectedAngle -= poiSign * 5.f;
	}
	else if (pcImpact > 50.f)
	{
		reflectedAngle -= poiSign * 2.5f;
	}
	else if (pcImpact > 10.f)
	{
		reflectedAngle -= poiSign * 1.f;
	}

	reflectedAngle = (reflectedAngle > 155.f && reflectedAngle < 270.f) ? 155.f : reflectedAngle;
	reflectedAngle = (reflectedAngle < 30.f && reflectedAngle > 270.f) ? 30.f : reflectedAngle;
	reflectedAngle = (reflectedAngle == 270.f) ? 90.f : reflectedAngle;

	normVel.x = sin(reflectedAngle);
	normVel.y = abs(cos(reflectedAngle));
	currVel = normVel * speed;
	currVel.z = 0.f;

	ballObj->GetVelocity()->SetPosition(currVel);
}