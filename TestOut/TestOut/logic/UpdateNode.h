#ifndef LOGIC_UPDATE
#define LOGIC_UPDATE

#include "..\SceneNode.h"

class UpdateNode
{
public:
	virtual void HandleUpdate(SceneNode *node, float deltaTime) = 0;
};

#endif // !LOGIC_UPDATE
