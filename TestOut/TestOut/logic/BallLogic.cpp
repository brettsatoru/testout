#include "BallLogic.h"

BallUpdate::BallUpdate(Transform *velocity)
{
	pVelocity = velocity;
	mLaunched = false;
	pTrackedObject = 0;
}

void BallUpdate::HandleUpdate(SceneNode *node, float dt)
{
	if (!GetIsLaunched() && pTrackedObject != NULL)
	{
		BoundingBox ours = node->GetBoundingBox();
		BoundingBox trackedBox = pTrackedObject->GetBoundingBox();
		glm::vec3 trackPosition(1.f);
		trackPosition.y = trackedBox.lower_left.y + trackedBox.height + (ours.height / 2) + 2.f;
		trackPosition.x = trackedBox.center.x;

		node->transform()->SetPosition(trackPosition);
	}
	else
	{
		node->transform()->Move(pVelocity->GetPosition() * dt);
	}
}

Transform *BallUpdate::GetVelocity()
{
	return pVelocity;
}

bool BallUpdate::GetIsLaunched()
{
	return mLaunched;
}

void BallUpdate::SetLaunched(bool value)
{
	mLaunched = value;
}

SceneNode *BallUpdate::GetTrackedObject()
{
	return pTrackedObject;
}

void BallUpdate::SetTrackedObject(SceneNode *tNode)
{
	pTrackedObject = tNode;
}

BallCollide::BallCollide(Transform *velocity)
{
	pVelocity = velocity;
}

void BallCollide::HandleCollisionEvent(ColliderNode *self, ColliderNode *other, glm::vec3 intersectPoint)
{
	// Does nothing, the ball makes zero determinations on its own.
}

Transform *BallCollide::GetVelocity()
{
	return pVelocity;
}
