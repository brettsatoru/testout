#ifndef LOGIC_COLLIDER_NODE
#define LOGIC_COLLIDER_NODE

#include "../SceneNode.h"
#include "../glm/glm.hpp"

class ColliderNode
{
private:
	SceneNode *viewNode;
public:
	ColliderNode();
	virtual void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3) = 0;

	SceneNode *GetSceneNode();
	void SetSceneNode(SceneNode *);
};

#endif // !LOGIC_COLLIDER_NODE
