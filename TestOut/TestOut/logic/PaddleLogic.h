#ifndef PADDLE_LOGIC
#define PADDLE_LOGIC

#include "ColliderNode.h"
#include "UpdateNode.h"
#include "..\SceneNode.h"

class PaddleUpdate : public UpdateNode
{
private:
	double *mouseX;
public:
	PaddleUpdate(double *);
	void HandleUpdate(SceneNode *, float);
};

class PaddleCollide : public ColliderNode
{
public:
	void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3);
};

#endif