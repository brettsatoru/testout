#include "BrickLogic.h"
#include "BallLogic.h"

void BrickCollide::HandleCollisionEvent(ColliderNode *self, ColliderNode *other, glm::vec3 intersectPoint)
{
	BallCollide* ballObj = dynamic_cast<BallCollide*>(other);

	if (ballObj == NULL) return;
	if (!self->GetSceneNode()->IsVisible()) return;

	self->GetSceneNode()->SetVisibility(false);

	glm::vec3 origin = self->GetSceneNode()->transform()->GetPosition();
	glm::vec3 ballPoint = ballObj->GetSceneNode()->transform()->GetPosition();
	float speed = glm::length(ballObj->GetVelocity()->GetPosition()) + 5.0f;

	glm::vec3 newDirection;
	newDirection.x = glm::distance(origin.x, ballPoint.x);
	newDirection.x = (origin.x > ballPoint.x) ? -newDirection.x : newDirection.x;
	newDirection.y = glm::distance(origin.y, ballPoint.y);
	newDirection.y = (origin.y > ballPoint.y) ? -newDirection.y : newDirection.y;

	newDirection = glm::normalize(newDirection) * speed;

	ballObj->GetVelocity()->SetPosition(newDirection);
}