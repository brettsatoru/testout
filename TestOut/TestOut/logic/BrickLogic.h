#ifndef BRICK_LOGIC
#define BRICK_LOGIC

#include "ColliderNode.h"

class BrickCollide : public ColliderNode
{
public:
	void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3);
};

#endif
