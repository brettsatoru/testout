#ifndef BORDER_LOGIC
#define BORDER_LOGIC

#include "ColliderNode.h"
#include "..\render\Transform.h"
#include "..\glm\glm.hpp"
#include "BallLogic.h"

class CeilingLogic : public ColliderNode
{
public:
	void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3);
};

class WallLogic : public ColliderNode
{
public:
	void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3);
};

class FloorLogic : public ColliderNode
{
private:
	BallUpdate *bUNode;
public:
	void HandleCollisionEvent(ColliderNode *, ColliderNode *, glm::vec3);
	void SetBallUpdateTarget(BallUpdate *);
};

#endif