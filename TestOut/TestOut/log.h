#pragma once

#include <mutex>

const int HeaderLength = 8;
const int LogThreadBufferSize = 4096;

struct Logger {
	~Logger();

	char *filePath;
	std::mutex logLock;
	std::thread *logThreads[LogThreadBufferSize];
	int numberOfThreads;
	bool logThreadsFinished[LogThreadBufferSize];

	void log(char *message);
	void log_line(char *message);
};

extern Logger logger;

struct LogData {
	std::mutex *logLock;
	char *filePath;
	char *message;
	size_t messageLength;
	bool freeMessage;
	int *numberOfThreads;
	bool *threadFinished;
};

enum LogLevel {
	Trace,
	Info,
	Debug,
	Warn,
	Error,
	Fatal
};

void log(LogLevel level, char *message);
void log_to_file(LogData data);
char *concat_strings(char *lhs, size_t lhs_size, char *rhs, size_t rhs_size);

#define LOG_TRACE(message) log(Trace, message)
#define LOG_INFO(message) log(Info, message)
#define LOG_DEBUG(message) log(Debug, message)
#define LOG_WARN(message) log(Warn, message)
#define LOG_ERROR(message) log(Error, message)
#define LOG_FATAL(message) log(Fatal, message)