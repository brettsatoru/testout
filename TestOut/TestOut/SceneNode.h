#ifndef SCENENODE
#define SCENENODE

#include <GL\glew.h>
#include <GL\GL.h>
#include "glm\glm.hpp"
#include "render\MeshRenderer.h"
#include "render\Transform.h"
#include "render\Material.h"
#include "BoundingBox.h"

// A class responsible for handling rendered entities in Graphic Space
// Not yet implemented, this is meant to be the beginning of the class so we can consolidate Physics and Rendering information into one place.
class SceneNode {
private:
	// Individual resources a SceneNode needs
	Transform *mTransform;
	MeshRenderer *mMeshRenderer;
	// We'll want to be able to indicate what sort of uniforms something like this has and how they're intended to be used later.
	// But baseline we need to assume we have a Model, View, and Proj uniform if it's being given to a SceneNode
	GLuint mShaderProgramId;
	Render::Material *mMaterial;

	bool mCollidable;
	bool mVisible;
	unsigned int mId;
public:
	// SceneNode(GLuint shaderProgram, Transform *transform = 0, MeshRenderer *renderer = 0);
	SceneNode(Transform *transform = 0, MeshRenderer *renderer = 0, Render::Material *material = 0);
	// Releases all taken memory.
	~SceneNode();

	// Getting individual systems we link to.
	Transform *transform();
	// May want to be able to set a new shared mesh but now allow editting on the current somehow.
	MeshRenderer sharedmesh();

	GLuint GetShaderId();
	GLuint SetShaderId(GLuint id);

	bool IsVisible();
	void SetVisibility(bool target);

	void Draw(glm::mat4 projection, glm::mat4 view, glm::mat4 parentModel=glm::mat4(1.f));
	
	BoundingBox GetBoundingBox();
	bool MaybeCollide(SceneNode *maybeCollidesWith, int x, int y);
	bool CollidesWith(SceneNode *otherNode);

	unsigned int GetNodeId();

	// Missing: Parent/Child Hierarchy
};

#endif // !SCENENODE