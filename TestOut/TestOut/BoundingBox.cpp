#include "BoundingBox.h"

bool BoundingBox::is_point_inside_box(float x, float y) {
	return x >= lower_left.x &&
		x <= lower_left.x + width &&
		y >= lower_left.y &&
		y <= lower_left.y + height;
}

bool BoundingBox::collides_with_other_box(BoundingBox other)
{
	return !(other.lower_left.x > this->lower_left.x + this->width
		|| other.lower_left.x + other.width < this->lower_left.x
		|| other.lower_left.y + other.height < this->lower_left.y
		|| other.lower_left.y > this->lower_left.y + this->height);
}