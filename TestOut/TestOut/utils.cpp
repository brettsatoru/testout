#include <string>
#include <stdlib.h>

#include "log.h"
#include "utils.h"

extern Logger logger;

char *concat_strings(char *lhs, size_t lhs_size, char *rhs, size_t rhs_size) {
	char *result = (char*)malloc(lhs_size + rhs_size);

	for (size_t index = 0; index < lhs_size; index++) {
		result[index] = lhs[index];
	}

	for (size_t index = 0; index < rhs_size; index++) {
		result[lhs_size + index] = rhs[index];
	}

	result[lhs_size + rhs_size] = '\0';

	return result;
}

const char *read_to_end(std::string filePath) {
	const char *fileContents;
	FILE *file = fopen(filePath.c_str(), "rb");

	if (file == NULL) {
		char buffer[256];
		auto err = errno;
		snprintf(buffer, 255, "Could not open the file: errno %d", err);
		printf(buffer);
	}
	else {
		fseek(file, 0, SEEK_END);
		auto fileLength = ftell(file);

		rewind(file);

		// We're probably never going to free this, as these contents live everywhere so we can reuse them.
		fileContents = (char*)calloc(fileLength + 1, 1);

		fread((void *)fileContents, 1, fileLength, file);

		fclose(file);
	}

	return fileContents;
}