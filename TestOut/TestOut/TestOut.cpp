// TestOut.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <fstream>
#include <string>
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtx\transform.hpp"
#include "render\MeshRenderer.h"
#include "render\Transform.h"
#include "render\Primitives.h"
#include "render\BaseMaterial.h"
#include "render\FlatColorMaterial.h"
#include "render\Material.h"
#include "SceneNode.h"
#include "logic\BallLogic.h"
#include "logic\BorderLogic.h"
#include "logic\PaddleLogic.h"
#include "logic\BrickLogic.h"
#include "GameObject.h"
#include <stack>
#include <vector>
#include <queue>
#include <chrono>
#include <Windows.h>

#include "log.h"
#include "utils.h"

Logger logger;

#pragma comment(lib, "OpenGL32.lib")

using namespace glm;

static void keycode_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
static void window_refresh_callback(GLFWwindow *window, int width, int height);
static void generate_grid(std::vector<Render::Material *> mats);
static void GenerateWalls(Render::Material *material, int windowWidth, int windowHeight);
static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

// These should probably be subsumed into some world map.
static std::vector< std::vector< GameObject > > brickgrid;
static std::vector<GameObject> walls;

void PrintSuccess(SceneNode *, SceneNode *, glm::vec3);
void WallCollision(SceneNode *wall, SceneNode *collidedWith, glm::vec3);

static std::queue<std::tuple<double, double>> button_presses;
static SceneNode playerPaddle;
static SceneNode ball;

static bool load_shaders(char *shaderFilePath, GLuint &shader_program, std::vector<void *> &shaders);

int main()
{
#ifdef NDEBUG
	ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif
	logger.filePath = "logout.txt";

	LOG_TRACE("Program Start");

	int statusCode = 0;

	if (glfwInit() == GL_TRUE)
	{
		std::vector<void *>shaders;

		// Initialize Window and Context
		GLFWwindow *window = glfwCreateWindow(800, 600, "Test Out!", NULL, NULL);
		glfwMakeContextCurrent(window);
		glfwSetKeyCallback(window, keycode_callback);
		glfwSetMouseButtonCallback(window, mouse_button_callback);
		glfwSetWindowSizeCallback(window, window_refresh_callback);
		glewInit();

		// Frame Buffer and Frame Swap Initialization
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		glViewport(0, 0, width, height);
		glfwSwapInterval(1);

		int clientWidth;
		int clientHeight;
		glfwGetWindowSize(window, &clientWidth, &clientHeight);

		GLuint shader_program = 0;
		if (!load_shaders("default.shader", shader_program, shaders)) {
			LOG_ERROR("Could not load shaders.");
		}

		GLuint flatcolor_shader_program = 0;
		if (!load_shaders("flatcolorshader.shader", flatcolor_shader_program, shaders))
		{
			LOG_ERROR("Could not load the flat color shader.");
		}

		double mouseX; // Track mouse position.

		BaseMaterial baseMat(shader_program);
		std::vector < Render::Material * > colorMats;
		FlatColorMaterial redMat(flatcolor_shader_program, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

		FlatColorMaterial orangeMat(flatcolor_shader_program, glm::vec4(1.0f, 0.5f, 0.0f, 1.0f));
		FlatColorMaterial yellowMat(flatcolor_shader_program, glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
		FlatColorMaterial greenMat(flatcolor_shader_program, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
		FlatColorMaterial tealMat(flatcolor_shader_program, glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
		FlatColorMaterial blueMat(flatcolor_shader_program, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
		FlatColorMaterial violetMat(flatcolor_shader_program, glm::vec4(0.5f, 0.0f, 1.0f, 1.0f));
		FlatColorMaterial magentaMat(flatcolor_shader_program, glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
		
		colorMats.push_back(&redMat);
		colorMats.push_back(&orangeMat);
		colorMats.push_back(&yellowMat);
		colorMats.push_back(&greenMat);
		colorMats.push_back(&tealMat);
		colorMats.push_back(&blueMat);
		colorMats.push_back(&violetMat);
		colorMats.push_back(&magentaMat);

		// Transform for testing Matrix Loading
		Transform transform = Transform(
			glm::vec3(100.f, 100.f, 0.f),
			glm::vec3(0, 0, 0),
			glm::vec3(10.f, 10.f, 1.0f));

		glm::mat4 perspective = glm::ortho(
			0.f,
			(float)width,
			0.f,
			(float)height,
			0.0f, 100.f);
		//glm::perspective(45.0f, 800.0f / 600.0f, 0.1f, 100.f);
		glm::mat4 camera = glm::lookAt(glm::vec3(0.f, 0.f, 1.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(0, 1, 0));

		MeshRenderer object = *GetQuad();

		GameObject playerPaddle;
		playerPaddle.GenerateSceneNode(
			GetQuad(),
			new Transform(
				glm::vec3(100.f, 100.f, 0.f),
				glm::vec3(0.f, 0.f, 0.f),
				glm::vec3(50.f, 10.f, 1.0f)),
			&baseMat);
		playerPaddle.SetUpdateLogic(new PaddleUpdate(&mouseX));
		playerPaddle.SetCollisionEnterLogic(new PaddleCollide());

		//playerPaddle.AddColliderOnEnter(PrintSuccess);

		GameObject ball;
		ball.GenerateSceneNode(GetQuad(), 
			new Transform(
				glm::vec3(100.f, 150.f, 0.f),
				glm::vec3(0.f, 0.f, 0.f),
				glm::vec3(5.f, 5.f, 1.0f)),
			&baseMat);
		BallUpdate buNode(
			new Transform(
				glm::vec3(50.0f, -25.f, 0.f),
				glm::vec3(0.f, 0.f, 0.f),
				glm::vec3(0.f, 0.f, 0.f)
			)
		);
		buNode.SetTrackedObject(playerPaddle.GetSceneNode());
		BallCollide bcNode(
			buNode.GetVelocity()
		);
		
		ball.SetUpdateLogic(&buNode);
		ball.SetCollisionEnterLogic(&bcNode);

		//ball.AddColliderOnEnter(PrintSuccess);

		generate_grid(colorMats);

		GenerateWalls(&baseMat, clientWidth, clientHeight);
		FloorLogic* fLogic = dynamic_cast<FloorLogic*>(walls[walls.size() - 1].GetCollisionEnterLogic());
		if (fLogic != NULL)
		{
			fLogic->SetBallUpdateTarget(&buNode);
		}

		std::chrono::high_resolution_clock::time_point currTime;
		std::chrono::duration<float> deltaTime;
		std::chrono::high_resolution_clock::time_point lastTime = std::chrono::high_resolution_clock::now();

		// Program/Game Loop
		while (!glfwWindowShouldClose(window))
		{
			glClearColor(0.f / 255.0f , 0.f / 255.f, 30.f / 255.f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			// Get Delta-time
			currTime = std::chrono::high_resolution_clock::now();
			deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(currTime - lastTime);

			// Update
			playerPaddle.Update(deltaTime.count());
			ball.Update(deltaTime.count());

			for (int r = 0; r < brickgrid.size(); r++)
			{
				for (int c = 0; c < brickgrid[r].size(); c++)
				{
					brickgrid[r][c].GetSceneNode()->Draw(perspective, camera);
					if (ball.GetSceneNode()->CollidesWith(brickgrid[r][c].GetSceneNode()))
					{
						brickgrid[r][c].CollisionEnter(&ball, glm::vec3(0.f));
						//ball.Update(deltaTime.count());
					}
				}
			}

			for (auto index = 0; index < walls.size(); index++) {
				walls[index].GetSceneNode()->Draw(perspective, camera);
				if (ball.GetSceneNode()->CollidesWith(walls[index].GetSceneNode()))
				{
					walls[index].CollisionEnter(&ball, glm::vec3(0.f));
				}
			}

			// Draw the player paddle
			playerPaddle.GetSceneNode()->Draw(perspective, camera);
			ball.GetSceneNode()->Draw(perspective, camera);

			if (ball.GetSceneNode()->CollidesWith(playerPaddle.GetSceneNode()))
			{
				playerPaddle.CollisionEnter(&ball, glm::vec3(0.f));
			}

			glfwSwapBuffers(window);

			lastTime = currTime;

			glfwPollEvents();

			double mouseY;
			glfwGetCursorPos(window, &mouseX, &mouseY);

			playerPaddle.LateUpdate(deltaTime.count());
			for (auto index = 0; index < walls.size(); index++)
			{
				walls[index].LateUpdate(deltaTime.count());
			}

			for (int r = 0; r < brickgrid.size(); r++)
			{
				for (int c = 0; c < brickgrid[r].size(); c++)
				{
					brickgrid[r][c].LateUpdate(deltaTime.count());
				}
			}

			while (!button_presses.empty()) {

				if (!buNode.GetIsLaunched())
				{
					buNode.GetVelocity()->SetPosition(glm::vec3(0.f, 150.f, 0.f));
					buNode.SetLaunched(true);
				}

				auto mouse_pos = button_presses.front();
				button_presses.pop();

				auto mouseClickX = std::get<0>(mouse_pos);
				auto mouseClickY = std::get<1>(mouse_pos);

				playerPaddle.GetSceneNode()->MaybeCollide(NULL, mouseClickX, clientHeight - mouseClickY);
				ball.GetSceneNode()->MaybeCollide(NULL, mouseClickX, clientHeight - mouseClickY);
				if (walls[2].GetSceneNode()->MaybeCollide(NULL, mouseClickX, clientHeight - mouseClickY))
				{
					LOG_INFO("Clicked Right Wall");
				}
			}
		}

		glfwDestroyWindow(window);

		glfwTerminate();
	}
	else
	{
		LOG_ERROR("An error occured while initializing glfw");
	}

	if (statusCode != 0)
	{
		// TODO: We need a proper logdumping method.
		LOG_ERROR("Errors detected on close, leaving open");
		system("pause");
	}

	LOG_TRACE("Program Complete");

    return statusCode;
}

static bool load_shaders(char *shaderFilePath, GLuint &shader_program, std::vector<void *> &shaders) {
	bool result = true;

	// Read in .shader file which points to other shaders.
	// Using strings because POSIX getline isn't available in C++
	// and I don't want to read from file one character at a time.
	std::string fragmentShaderPath;
	std::string vertexShaderPath;

	std::ifstream shaderFile(shaderFilePath);
	getline(shaderFile, fragmentShaderPath);
	getline(shaderFile, vertexShaderPath);

	shaderFile.close();

	const char *vertexShader = read_to_end(vertexShaderPath);
	shaders.push_back((void *)vertexShader);

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertexShader, NULL);
	glCompileShader(vs);

	GLint shCompileSuccess = 0;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &shCompileSuccess);

	if (shCompileSuccess == GL_FALSE)
	{
		LOG_ERROR("The vertex shader did not compile correctly");

		GLint shLogSize = 0;
		glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &shLogSize);

		std::vector<GLchar> errorLog(shLogSize);
		glGetShaderInfoLog(vs, shLogSize, &shLogSize, &errorLog[0]);

		LOG_ERROR(&errorLog[0]);
		
		result = false;
	}

	const char *fragmentShader = read_to_end(fragmentShaderPath);
	shaders.push_back((void *)fragmentShader);

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragmentShader, NULL);
	glCompileShader(fs);

	shCompileSuccess = 0;
	glGetShaderiv(fs, GL_COMPILE_STATUS, &shCompileSuccess);

	if (shCompileSuccess == GL_FALSE)
	{
		LOG_ERROR("The fragment shader did not compile correctly");

		GLint shLogSize = 0;
		glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &shLogSize);

		std::vector<GLchar> errorLog(shLogSize);
		glGetShaderInfoLog(fs, shLogSize, &shLogSize, &errorLog[0]);

		LOG_ERROR(&errorLog[0]);

		result = false;
	}

	if (shCompileSuccess == GL_FALSE)
	{
		LOG_ERROR("The fragment shader did not compile correctly");
		result = false;
	}

	shader_program = glCreateProgram();
	glAttachShader(shader_program, fs);
	glAttachShader(shader_program, vs);
	glLinkProgram(shader_program);

	return result;
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		double x, y;
		glfwGetCursorPos(window, &x, &y);

		printf("Got a button press at (%f, %f).\n", x, y);

		button_presses.push(std::make_tuple(x, y));
	}
}

static void keycode_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (playerPaddle.transform() != 0)
		{
			playerPaddle.transform()->Move(glm::vec3(-10.0f, 0.f, 0.f));
		}
	} else if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (playerPaddle.transform() != 0)
		{
			playerPaddle.transform()->Move(glm::vec3(10.0f, 0.f, 0.f));
		}
	}
}

static void window_refresh_callback(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void generate_grid(std::vector<Render::Material *> materials)
{
	int grid_width = 15;
	int grid_height = 10;

	GLfloat boxHeight = 20.0f;
	GLfloat boxHeightOff = 1.0f;
	GLfloat boxWidth = 40.0f;
	GLfloat boxWidthOff = 1.0f;

	GLfloat start_x = 105.f;
	GLfloat start_y = 300.f;

	for (int h = 0; h < grid_height; h++)
	{
		GLfloat currHeight = start_y + (boxHeight + boxHeightOff) * h;
		brickgrid.push_back(std::vector < GameObject >());
		for (int w = 0; w < grid_width; w++)
		{
			const int wrappedRow = h % materials.capacity();
			GLfloat currWidth = start_x + (boxWidth + boxWidthOff) * w;
			GameObject currObj;
			currObj.GenerateSceneNode(
				GetQuad(),
				new Transform(
					glm::vec3(currWidth + (boxWidth / 2.f), currHeight + (boxHeight / 2), 1.0f),
					glm::vec3(0.f),
					glm::vec3(boxWidth / 2, boxHeight / 2, 1.0f)),
				materials.at(wrappedRow));
			currObj.SetCollisionEnterLogic(new BrickCollide());
			brickgrid[h].push_back(currObj);
		}
	}
}

void GenerateWalls(Render::Material *material, int windowWidth, int windowHeight) {

	GameObject ceiling;
	ceiling.GenerateSceneNode(GetQuad(),
		new Transform(
			glm::vec3((float)windowWidth / 2.f, (float)windowHeight - 5.f, 1.f),
			glm::vec3(0.f),
			glm::vec3((float)windowWidth, 10.f, 1.f)
		),
		material);
	ceiling.SetCollisionEnterLogic(new CeilingLogic());

	GameObject leftWall;
	leftWall.GenerateSceneNode(GetQuad(),
		new Transform(
			glm::vec3(5.f, (float)windowHeight/2.f, 1.f),
			glm::vec3(0.f),
			glm::vec3(10.f, (float)windowHeight, 1.f)),
		material);
	leftWall.SetCollisionEnterLogic(new WallLogic());

	GameObject rightWall;
	rightWall.GenerateSceneNode(GetQuad(),
		new Transform(
			glm::vec3(windowWidth-5.f, (float)windowHeight/2.f, 1.f),
			glm::vec3(0.f),
			glm::vec3(10.f, (float)windowHeight, 1.f)
		),
		material);
	rightWall.SetCollisionEnterLogic(new WallLogic());

	GameObject floor;
	floor.GenerateSceneNode(GetQuad(),
		new Transform(
			glm::vec3((float)windowWidth / 2.f, 5.f, 1.f),
			glm::vec3(0.f),
			glm::vec3((float)windowWidth, 10.f, 0.f)
		),
		material);
	floor.SetCollisionEnterLogic(new FloorLogic());

	walls.push_back(ceiling);
	walls.push_back(leftWall);
	walls.push_back(rightWall);
	walls.push_back(floor);
}

void PrintSuccess(SceneNode *it, SceneNode *collidedWith, glm::vec3 pointOfCollision) {
	auto pos = it->transform()->GetPosition();
	printf("You clicked inside the object at position (%f, %f)!\n", pos.x, pos.y);
}