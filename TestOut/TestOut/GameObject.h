#ifndef GAME_OBJECT
#define GAME_OBJECT

#include "SceneNode.h"
#include "BoundingBox.h"
#include "render\MeshRenderer.h"
#include "render\Transform.h"
#include "logic\UpdateNode.h"
#include "logic\ColliderNode.h"
#include "render\Material.h"
#include <map>

// This class is the container of basic events a logical game object has.
class GameObject
{
private:
	SceneNode *pSceneNode;

	UpdateNode *pUpdate;
	ColliderNode *pColliderEnter;
	ColliderNode *pColliderExit;

	Transform *pVelocity;

	unsigned int mId;
	std::map<unsigned int, GameObject *> mCollidingObjects;
protected:

public:
	GameObject(); // Blank Constructor

	unsigned int GetGameObjectId();

	void Update(float);
	void LateUpdate(float);
	void CollisionEnter(GameObject *other, glm::vec3 intersectPoint);
	void CollisionExit(GameObject *other, glm::vec3 intersectPoint);

	// Generates new scene node from MeshRenderer and Transform pointers.
	// Returns pointer to old scene node, if any were replaced.
	SceneNode *GenerateSceneNode(MeshRenderer *, Transform *, Render::Material * = 0);

	SceneNode *GetSceneNode();
	// Returns old scene node pointer if one is replaced.
	SceneNode *SetSceneNode(SceneNode *);

	// Returns pointer to old update logic if one is replaced.
	UpdateNode *SetUpdateLogic(UpdateNode *);

	ColliderNode *GetCollisionEnterLogic();
	// Returns pointer to old collision enter logic if one is replaced.
	ColliderNode *SetCollisionEnterLogic(ColliderNode *);

	ColliderNode *GetCollisionExitLogic();
	// Returns pointer to old collision exit logic if one is replaced.
	ColliderNode *SetCollisionExitLogic(ColliderNode *);

	Transform *GetVelocity();
};

#endif